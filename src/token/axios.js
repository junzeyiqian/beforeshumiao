
import axios from 'axios'; // 引入 axios 库，用于进行 HTTP 请求
import axiosRetry from 'axios-retry'; // 引入 axios-retry 库，用于实现请求重试机制
import router from '../router' // 引入 Vue Router 实例，用于页面导航

// 创建一个 axios 实例
export function createAxios(option = {}) {
    return axios.create({
        ...option, // 将传入的选项合并到 axios 实例中
    });
}


// 创建一个名为 houseApi 的 axios 实例，设置基本 URL 和超时时间
export const houseApi = createAxios({
    baseURL: 'http://localhost:3000', // 设定基础 URL
    timeout: 5000, // 请求超时时间设置为 5000 毫秒（5 秒）
});


//   请求拦截器

houseApi.interceptors.request.use(
    async function (config) {   
        console.log('开始请求'); // 打印请求开始信息
        const accessToken = localStorage.getItem('accessToken'); // 从 localStorage 获取 accessToken
        const refreshToken = localStorage.getItem('refreshToken'); // 从 localStorage 获取 refreshToken
        // console.log(accessToken,refreshToken);
        
        // if(accessToken){
        //     config.headers['Authorization']="Bearer"+accessToken;
        // }
        config.headers.accessToken = accessToken ; // 设置请求头中的 accessToken
        config.headers.refreshToken = refreshToken; // 设置请求头中的 refreshToken
        // console.log(config.headers);
        
        return config; // 返回配置
    },
    function (error) {
        return Promise.reject(error); // 拒绝请求错误
    }
);


// 响应拦截器
houseApi.interceptors.response.use(
    async function (response){
        console.log(response,'response')
        if(response.data.code==401){
            console.log(response.data.code);
            let res=refreshToken() 
            console.log(res,'resres');
        }
        return response
        
    },
    function(error){
        return Promise.reject(error)
    }
)


// houseApi.interceptors.response.use(
//    async(response)=>{
//     console.log(response,'response');
//     return response
//    },
//    async(error)=>{
//     console.log(error,'error');
//     let {status,data,config}=error.response;
//     if(status===401 && !config.url.includes('/refresh')){
//         // console.log(response.data.code,'response');
        
//         const res=await refreshToken()  //刷新token
//         if(res.status===200){
//             return houseApi(config)  //重新发送请求
//         }else{
//             alert(data.msg ||'登录过期，请从新登录');
//             return {}
//         }
//     }else{
//         return error.response
//     }
//    }
// )



// export const refreshToken=()=>{
//     const r_tk=localStorage.getItem('refreshToken')
//     console.log(r_tk,'r_tk');
//     if(r_tk){
//         let result=houseApi.get('/users/refresh',{headers:{refreshToken:r_tk}})
//         console.log(result,'result');
//         if(result.data.code==401){
//             localStorage.removeItem('refreshToken')
//             return {}
//         }else if(result.data.code==200){
//             localStorage.setItem('accessToken',res.data.accessToken)
//             localStorage.setItem('refreshToken',res.data.refreshToken)
//            return result.data  //返回新的令牌
//         }
        
//     }else{
//         return {}
//     }
    
// }



// 刷新token
export const refreshToken = () => {
    return new Promise((resolve, reject) => {
      const r_tk = localStorage.getItem('refreshToken');
      console.log(r_tk,'r_tk');
      if (r_tk) {
        houseApi.get('/refresh', { headers: { refreshToken: r_tk } })
          .then((res) => {
            console.log(res.data.code, 'res.data.code');
            if (res.data.code === 401) {
              localStorage.removeItem('refreshToken');
              reject('Token has expired'); // 如果 token 过期，拒绝 Promise
            //   router.push('/index/login');
            } else if (res.data.code === 200) {
              console.log(res.data, '200');
              localStorage.setItem('accessToken', res.data.accessToken);
              localStorage.setItem('refreshToken', res.data.refreshToken);
              resolve(res.data.code); // 如果成功，解析 Promise 并返回 res.data.code
            }
          })
          .catch((error) => {
            console.error('Error during refresh token:', error);
            reject(error); // 如果有错误发生，拒绝 Promise
          });
      } else {
        reject('No refresh token available'); // 如果没有 refresh token，拒绝 Promise
      }
    });
  };
export default houseApi; // 导出 houseApi 实例

