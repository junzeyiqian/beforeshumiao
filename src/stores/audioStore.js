import { defineStore } from 'pinia';

export const useAudioStore = defineStore('audio', {
  state: () => ({
    currentTime: 0,
    isPlaying: false,
    audio: null, 
  }),
  actions: {
    initAudio(url) {
      this.audio = new Audio(url);
      this.audio.addEventListener('timeupdate', this.updateCurrentTime);
      this.audio.addEventListener('pause', this.updateCurrentTime);
      // 其他事件监听...
    },
    updateCurrentTime() {
      this.currentTime = this.audio.currentTime;
      localStorage.setItem('audio:currentTime', this.currentTime);
    },
    play() {
      if (this.audio) {
        this.audio.play();
        this.isPlaying = true;
      }
    },
    pause() {
      if (this.audio) {
        this.audio.pause();
        this.isPlaying = false;
      }
    },
    resumePlayback() {
      if (this.audio) {
        this.audio.currentTime = this.currentTime || localStorage.getItem('audio:currentTime') || 0;
        this.play();
      }
    },
  }
});