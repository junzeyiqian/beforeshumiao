import { createRouter, createWebHistory } from 'vue-router';
const routes = [
    {
        path: '/', // 根路径
        component: () => import('../views/Home.vue')
    },
    {
        path: '/info', // 根路径
        name: 'Info',
        component: () => import('../views/explore/Info.vue')
    },
    {
        name: 'Home',
        component: () => import('../views/Home.vue')
    },
    {
        path: '/login', // 登录页面路径
        name: 'login',
        component: () => import('../views/Login.vue')
    },
    {
        component: () => import('../views/Login.vue')
    },
    {
        path: '/tribe',
        component: () => import('../views/Tribe.vue')
    },
    {
        path: '/addpost',
        component: () => import('../views/AddPost.vue')
    },
    {
        path: '/actdetail',
        component: () => import('../views/ActDetail.vue')
    },
    {
        path: '/postdetail',
        component: () => import('../views/PostDetail.vue')
    },
    {
        path: '/choose',
        name: 'choose',
        component: () => import('../views/explore/Choose.vue')
    },
    {
        path: '/music', 
        name: 'music',
        component: () => import('../views/explore/Music.vue')
    },
    {
        path: '/recharge', 
        name: 'recharge',
        component: () => import('../views/explore/Recharge.vue')
    },
    {
        path: '/tan', 
        name: 'tan',
        component: () => import('../views/explore/Tan.vue')
    },
    {
        path: '/pl', 
        name: 'pl',
        component: () => import('../views/explore/Context.vue')
    },

    //树洞模块
    {
        path: "/shudong",
        name: "树洞",
        component: () => import("../views/shudong/shudong.vue")
    },
    {
        path: "/setzh",
        name: "账号设置",
        component: () => import("../views/shudong/SetZh.vue")
    },
    {
        path: "/invite",
        name: "邀请好友",
        component: () => import("../views/shudong/InviteFriends.vue")
    },
    {
        path: "/message",
        name: "留言板",
        component: () => import("../views/shudong/LeaveMessage.vue")
    },
    {
        path: "/speaker",
        name: "发言板",
        component: () => import("../views/shudong/Speaker.vue")
    },
    {
        path:'/colleat',
        name:'收藏',
        component:()=>import('../views/shudong/Colleat.vue'),
    },
    {
        path:'/member',
        name:'会员',
        component:()=>import('../views/Members/Member.vue')
    },
    {
        path:'/agrmember',
        name:'会员协议',
        component:()=>import('../views/Agreements/Agrmember.vue')
    },
    {
        path:"/agrpayment",
        name:"支付协议",
        component:()=>import('../views/Agreements/Agrpayment.vue')
    },
    {
        path:"/agreement",
        name:"用户协议",
        component:()=>import('../views/Agreements/Agreement.vue')
    },
    //-------------------------树洞模块-------------------------
    {
        path:'/chat',
        name:'chat',
        component:()=>import('../views/Chat.vue')
    },
    {
        path:'/chatCon',
        name:'chatCon',
        component:()=>import('../views/ChatContent.vue')
    },
    {
        path:'/ai',
        name:'ai',
        component:()=>import('../views/AI.vue')
    },
    {
        path: '/start',
        name: 'start',
        component: () => import('../views/Start.vue')
    },
    {
        path:'/facelogin',
        name:'facelogin',
        component: () => import('../views/FaceLogin.vue')
    },
    {
        path:'/register',
        name:'register',
        component: () => import('../views/Register.vue')
    },
    {
        path:'/phonelogin',
        name:'phonelogin',
        component: () => import('../views/PhoneLogin.vue')
    },
    

    //测试页面
    {
        path:'/my',
        name:'my',
        component: () => import('../views/My.vue'),
        meta: { requiresAuth: true },
    },
    {
        path:'/test',
        name:'test',
        component:() => import('../pages/Test.vue'),
        meta: { requiresAuth: true },
    },
];

const router = createRouter({
    history: createWebHistory(), // 使用 HTML5 历史模式
    routes, // 路由配置
});

router.beforeEach((to,from,next) => {
    const token = localStorage.getItem('refreshToken');
    if(to.matched.some(record => record.meta.requiresAuth) && !token){
        next('/login');

    }else{
        next();
    }
})

export default router;