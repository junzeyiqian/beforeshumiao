// src/utils/axios.js

import axios from 'axios';

// 创建axios实例
const instance = axios.create({
  baseURL: process.env.VUE_APP_API_BASE_URL, // 从环境变量中获取API的基础URL
  timeout: 10000, // 请求超时时间
});

// 请求拦截器
instance.interceptors.request.use(
  config => {
    // 在这里添加任何全局的请求处理，例如添加token或加密参数
    // config.headers['Authorization'] = `Bearer ${yourToken}`;
    return config;
  },
  error => {
    // 处理请求错误
    return Promise.reject(error);
  }
);

// 响应拦截器
instance.interceptors.response.use(
  response => {
    // 任何HTTP状态码为2xx的响应数据都会触发此函数
    return response;
  },
  error => {
    // 任何超出2xx的HTTP状态码都会触发此函数
    // 你可以在这里处理错误，例如token过期、网络错误等
    if (error.response) {
      // 服务器返回的状态码不在2xx的范围
      // 处理响应错误，例如token过期、数据验证错误等
      switch (error.response.status) {
        case 401:
          // 处理未授权的情况
          break;
        case 403:
          // 处理拒绝访问的情况
          break;
        // ...其他错误处理
      }
    }
    return Promise.reject(error);
  }
);

export default instance;