import { createApp } from 'vue'
import { createPinia } from 'pinia'
import App from './App.vue'
import ArcoVue from '@arco-design/web-vue';
import '@arco-design/web-vue/dist/arco.css';
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import router from './router'
import Vant from 'vant';
import VueQrcode from 'vue-qrcode'
import NutUI from "@nutui/nutui";
import "@nutui/nutui/dist/style.css";
const app = createApp(App)
const pinia = createPinia()
import { IconFont } from '@nutui/icons-vue'
import '@nutui/icons-vue/dist/style_iconfont.css'
app.component('vue-qrcode', VueQrcode)
app.use(ElementPlus)
app.use(Vant);
app.use(pinia)
app.use(ArcoVue);
app.use(NutUI)
app.use(router)
app.use(IconFont)
app.config.performance = true; // 开启性能监测
app.mount('#app')
