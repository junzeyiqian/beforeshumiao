export const download = (url: string, name: string) => {
    const aTag = document.createElement('a')
    document.body.appendChild(aTag)
    aTag.href = url
    aTag.download = name
    aTag.click()
    aTag.remove()
}