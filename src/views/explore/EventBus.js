// eventBus.js
import { reactive, readonly } from 'vue';

const state = reactive(new EventTarget());

export const eventBus = readonly(state);