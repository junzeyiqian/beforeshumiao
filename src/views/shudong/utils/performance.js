
export function mark(name) {
  // process.env.NODE_ENV是一个环境变量，一般在node.js环境中使用，用于区分是开发环境，还是生产环境。
  // 开发环境中值为developent，生产环境为production
  if (process.env.NODE_ENV !== 'production' && window.performance.mark) {
    // 如果变量值不为生产环境并且浏览器支持performance.mark方法，则执行下面的代码
    window.performance.mark(name);
    // 这行代码会在浏览器中标记一个名为name的时间点。
  }
}


export function measure(Onm, startMark, endMark) {
  if (process.env.NODE_ENV !== 'production' && window.performance.measure) {
    performance.clearMeasures(Onm)
    // window.performance.measure是浏览器提升性能的Api，用于测量两个标记点之间的性能。
    window.performance.measure(Onm, startMark, endMark);
    // 获取测量结果并打印到控制台
    const measures = performance.getEntriesByName(Onm, 'measure');
    if (measures.length > 0) {
      let measure = measures[0];
      let startTime = measure.startTime.toFixed(2)
      let duration = measure.duration.toFixed(2)
      let totalTime = window.performance.now().toFixed(2)
      // 从页面开始加载开始的某个时间点到另一个时间点之间的时间差
      console.log('本次测量开始时间为' + startTime)
      console.log('此次页面性能测量时间为' + duration + 'ms');
      console.log('本次测量结束时间为' + totalTime);
    }
  }
}